package com.yhzl.two.shop.common;

/**
 * @program: pokemon
 * @author: chsm
 * @create: 2018-09-04 08:55
 **/
public class Const {

    public static final String LOGIN_USER = "loginUser";
    public static final String LOGIN_MANAGER = "loginManager";
    public static final String NOT_LOGIN = "未登录";
    public static final String FILE_PATH = "F:\\myImg\\";

}
