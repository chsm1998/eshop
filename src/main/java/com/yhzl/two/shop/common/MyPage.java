package com.yhzl.two.shop.common;

import lombok.Data;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-05 09:46
 **/
@Data
public class MyPage {

    private Integer currPage;
    private Integer pageSize;

}
