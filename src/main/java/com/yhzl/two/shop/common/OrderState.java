package com.yhzl.two.shop.common;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-11 14:48
 **/
public enum  OrderState {
    /**
     *
     */
    PLANCE_AN_ORDER("已下单", 1),
    DELIVERED("已发货", 2),
    SUCCESS("已完成", 3),
    REFUNDED("已退款", 4),
    REFUNDING("退款中", 5),
    ACCOUNT_PAID("已付款", 6);

    private String msg;
    private Integer code;

    OrderState(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }
}
