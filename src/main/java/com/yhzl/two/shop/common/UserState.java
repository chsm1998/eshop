package com.yhzl.two.shop.common;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-04 09:35
 **/
public enum  UserState {
    /**
     *
     */
    REGISTER(0, "已注册");

    private int code;
    private String msg;

    UserState(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public int getCode() {
        return code;
    }
}
