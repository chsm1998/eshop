package com.yhzl.two.shop.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chsm
 */
public class GeneratorMybatisPlusConfig {

    public static void main(String[] args) {
        // 自定义需要填充的字段
        List<TableFill> tableFillList = new ArrayList<>();
        tableFillList.add(new TableFill("gmt_create", FieldFill.INSERT));
        tableFillList.add(new TableFill("gmt_modified", FieldFill.INSERT_UPDATE));
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator()
                .setGlobalConfig(
                        // 全局配置
                        new GlobalConfig()
                                //输出目录
                                .setOutputDir("C:\\Users\\chsm\\IdeaProjects\\shop\\src\\main\\java")
                                // 是否覆盖文件
                                .setFileOverride(false)
                                // 开启 activeRecord 模式
                                .setActiveRecord(false)
                                // XML 二级缓存
                                .setEnableCache(false)
                                // XML ResultMap
                                .setBaseResultMap(true)
                                // XML columList
                                .setBaseColumnList(true)
                                .setSwagger2(true)
                                .setOpen(false)
                                .setAuthor("chsm")).setDataSource(
                        // 数据源配置
                        new DataSourceConfig()
                                // 数据库类型
                                .setDbType(DbType.MYSQL)
                                .setDriverName("com.mysql.jdbc.Driver")
                                .setUsername("three")
                                .setPassword("team")
                                .setUrl("jdbc:mysql:///two_shop?characterEncoding=utf8"))
                .setStrategy(
                        // 策略配置
                        new StrategyConfig()
                                // 表名生成策略
                                .setNaming(NamingStrategy.underline_to_camel)
                                .setTableFillList(tableFillList)
                                .setEntityBooleanColumnRemoveIsPrefix(true)
                                .setEntityLombokModel(true)
                                .setInclude("shop_cart")
                                .setRestControllerStyle(true))
                .setPackageInfo(
                        // 包配置
                        new PackageConfig()
                                .setParent("com.yhzl.two.shop"));
        mpg.execute();
    }

}
