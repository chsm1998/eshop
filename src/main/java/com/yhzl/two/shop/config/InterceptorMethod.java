package com.yhzl.two.shop.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.ServerResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-04 21:32
 **/
class InterceptorMethod {

    static void setResponse(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/json; charset=utf-8");
    }

    static boolean checkSession(HttpServletResponse response, Object manager, boolean isManager) throws Exception {
        if (manager == null) {
            // 返回未登录状态
            ObjectMapper objectMapper = new ObjectMapper();
            String s = null;
            if (isManager) {
                s = objectMapper.writeValueAsString(ServerResponse.createManagerNeedLogin(Const.NOT_LOGIN));
            } else {
                s = objectMapper.writeValueAsString(ServerResponse.createNeedLogin(Const.NOT_LOGIN));
            }
            response.getWriter().write(s);
            return  false;
        }
        return true;
    }

}
