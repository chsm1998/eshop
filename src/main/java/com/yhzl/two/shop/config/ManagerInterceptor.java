package com.yhzl.two.shop.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Manager;
import com.yhzl.two.shop.entity.User;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 后台管理登录校验
 * @program: shop
 * @author: chsm
 * @create: 2018-09-04 21:28
 **/
public class ManagerInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        System.out.println(session.getId());
        InterceptorMethod.setResponse(request, response);
        Manager manager = (Manager) session.getAttribute(Const.LOGIN_MANAGER);
        return InterceptorMethod.checkSession(response, manager, true);
    }
}
