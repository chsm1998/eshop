package com.yhzl.two.shop.config;

import com.yhzl.two.shop.common.Const;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: pokemon
 * @author: chsm
 * @create: 2018-08-12 12:21
 **/
@Configuration
public class MyWebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加前台登录拦截器
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/*/front/**")
                .excludePathPatterns("/error", "/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**");
        // 添加后台登录拦截器
        registry.addInterceptor(new ManagerInterceptor())
                .addPathPatterns("/*/after/**")
                .excludePathPatterns("/manager/**", "/error", "/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 配置图片资源映射
        registry.addResourceHandler("/myImg/**")
                .addResourceLocations("file:" + Const.FILE_PATH);
    }
}
