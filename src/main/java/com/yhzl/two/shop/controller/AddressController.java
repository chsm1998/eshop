package com.yhzl.two.shop.controller;


import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Address;
import com.yhzl.two.shop.entity.User;
import com.yhzl.two.shop.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@RestController
@RequestMapping("/address")
public class AddressController {

    private final IAddressService addressService;

    @Autowired
    public AddressController(IAddressService addressService) {
        this.addressService = addressService;
    }

    @PostMapping("/front/address")
    public ServerResponse<String> add(@RequestBody Address address, @SessionAttribute(Const.LOGIN_USER) User user) {
        return addressService.add(address.setUid(user.getId()));
    }

    @DeleteMapping("/front/address")
    public ServerResponse<String> delete(Integer id) {
        return addressService.delete(id);
    }

    @PutMapping("/front/address")
    public ServerResponse<String> update(@RequestBody Address address) {
        return addressService.update(address);
    }

    @GetMapping("/front/queryAll")
    public ServerResponse<List<Address>> queryAll(@SessionAttribute(Const.LOGIN_USER)User user) {
        return addressService.queryAll(user.getId());
    }

}

