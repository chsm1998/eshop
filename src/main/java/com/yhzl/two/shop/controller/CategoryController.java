package com.yhzl.two.shop.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yhzl.two.shop.common.MyPage;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Category;
import com.yhzl.two.shop.service.ICategoryService;
import com.yhzl.two.shop.vo.CategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    private final ICategoryService categoryService;

    @Autowired
    public CategoryController(ICategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/public/categorys")
    public ServerResponse<List<Category>> queryAll() {
        return ServerResponse.createSuccessData("获取类别成功", categoryService.list(null));
    }

    @PostMapping("/after/category")
    public ServerResponse<String> add(@RequestBody Category category) {
        return categoryService.add(category);
    }

    @DeleteMapping("/after/category")
    public ServerResponse<String> delete(Integer id) {
        return categoryService.delete(id);
    }

    @PutMapping("/after/category")
    public ServerResponse<String> update(@RequestBody Category category) {
        return categoryService.update(category);
    }

    @PostMapping("/after/categorys")
    public ServerResponse<IPage<Category>> queryAll(@RequestBody CategoryVo categoryVo) {
        return categoryService.queryPage(categoryVo);
    }

    @PostMapping("/after/byName")
    public ServerResponse<List<Category>> queryByName(@RequestBody Category category) {
        return categoryService.queryByName(category.getName());
    }

    @GetMapping("/after/category")
    public ServerResponse<Category> queryById(Integer id) {
        return categoryService.queryById(id);
    }
}

