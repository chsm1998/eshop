package com.yhzl.two.shop.controller;


import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.ResponseCode;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Manager;
import com.yhzl.two.shop.service.IManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@RestController
@RequestMapping("/manager")
public class ManagerController {

    private final IManagerService managerService;

    @Autowired
    public ManagerController(IManagerService managerService) {
        this.managerService = managerService;
    }

    @PostMapping("/after/login")
    public ServerResponse<Manager> login(@RequestBody Manager manager, HttpSession session) {
        ServerResponse<Manager> login = managerService.login(manager);
        if (login.getStatus() == ResponseCode.SUCCESS.getCode()) {
            session.setAttribute(Const.LOGIN_MANAGER, login.getData());
        }
        return login;
    }

    @GetMapping("/after/manager")
    public ServerResponse<Manager> getLogin(HttpSession session) {
        Manager manager = (Manager) session.getAttribute(Const.LOGIN_MANAGER);
        if (manager == null) {
            return ServerResponse.createManagerNeedLogin("未登录");
        }
        return ServerResponse.createSuccessData("获取登录信息成功", manager);
    }
}

