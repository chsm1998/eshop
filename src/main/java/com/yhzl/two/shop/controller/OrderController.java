package com.yhzl.two.shop.controller;


import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.OrderState;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Order;
import com.yhzl.two.shop.entity.User;
import com.yhzl.two.shop.service.AliService;
import com.yhzl.two.shop.service.IOrderItemService;
import com.yhzl.two.shop.service.IOrderService;
import com.yhzl.two.shop.vo.OrderStateVo;
import com.yhzl.two.shop.vo.ShopCartVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    private final IOrderService orderService;

    @Autowired
    public OrderController(IOrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/front/create")
    public ServerResponse<Map<String, Boolean>> createOrder(@RequestBody ShopCartVo shopCartVo, @SessionAttribute(Const.LOGIN_USER)User user) {
        return orderService.createOrder(shopCartVo, user.getId());
    }

    @GetMapping("/front/queryAll")
    public ServerResponse<List<Order>> queryByUid(@SessionAttribute(Const.LOGIN_USER) User user) {
        return orderService.getOrderByUid(user.getId());
    }

    @PutMapping("/front/updateState")
    public ServerResponse<String> frontUpdateState(@RequestBody OrderStateVo orderStateVo) {
        return orderService.updateState(orderStateVo.getOid(), orderStateVo.getState());
    }

    @GetMapping("/after/queryAll")
    public ServerResponse<List<Order>> queryAll() {
        return orderService.getOrderByUid(null);
    }

}

