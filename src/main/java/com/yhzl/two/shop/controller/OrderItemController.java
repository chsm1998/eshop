package com.yhzl.two.shop.controller;


import com.alipay.api.response.AlipayTradeQueryResponse;
import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.User;
import com.yhzl.two.shop.service.AliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@RestController
@RequestMapping("/orderItem")
public class OrderItemController {

    private final AliService aliService;

    @Autowired
    public OrderItemController(AliService aliService) {
        this.aliService = aliService;
    }

    @GetMapping("/getAlipayTradePagePay")
    public ServerResponse<String> getAlipayTradePagePay(String oid, @SessionAttribute(Const.LOGIN_USER)User user) {
        return aliService.alipayTradePagePayService(oid, user.getId());
    }

    @GetMapping("/isPay")
    public ServerResponse<AlipayTradeQueryResponse> isPay(String oid) {
        return aliService.alipayTradeQueryService(oid);
    }

}

