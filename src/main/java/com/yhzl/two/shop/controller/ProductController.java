package com.yhzl.two.shop.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Product;
import com.yhzl.two.shop.service.IProductService;
import com.yhzl.two.shop.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    private final IProductService productService;

    @Autowired
    public ProductController(IProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/after/queryPage")
    public ServerResponse<IPage<Product>> queryPage(@RequestBody ProductVo productVo) {
        return productService.queryPage(productVo);
    }

    @GetMapping("/public/product")
    public ServerResponse<Product> publicQueryById(Integer id) {
        return productService.queryById(id);
    }

    @PostMapping("/public/queryByDescAndCid")
    public ServerResponse<List<Product>> publicQueryByDescAndCid(@RequestBody Product product) {
        return productService.queryByDescAndCid(product);
    }

    @PostMapping("/public/products")
    public ServerResponse<List<Product>> queryAll() {
        return ServerResponse.createSuccessData("获取商品信息成功", productService.list(null));
    }

    @PostMapping("/after/queryByName")
    public ServerResponse<List<Product>> queryByName(@RequestBody Product product) {
        return productService.queryByName(product.getName());
    }

    @DeleteMapping("/after/delete")
    public ServerResponse<String> deleteById(Integer id) {
        return productService.deleteById(id);
    }

    @PostMapping("/after/update")
    public ServerResponse<String> updateById(Product product, MultipartFile file) {
        if (file != null) {
            upload(file, product);
        }
        return productService.update(product);
    }

    @PostMapping("/after/upload")
    public ServerResponse<String> uploadImg(Product product, MultipartFile file) {
        upload(file, product);
        return productService.add(product);
    }

    @PostMapping("/after/updateState")
    public ServerResponse<String> updateState(@RequestBody Product product) {
        return productService.update(product);
    }

    private void upload(MultipartFile file, Product product) {
        if (!file.isEmpty()) {
            String contentType = file.getContentType();
            String fileName = file.getOriginalFilename();
            System.out.println(contentType);
            System.out.println(fileName);
            String filePath = Const.FILE_PATH;
            File targetFile = new File(filePath);
            System.out.println(filePath);
            if (!targetFile.exists()) {
                targetFile.mkdirs();
            }
            FileOutputStream out = null;
            String newFileName = System.currentTimeMillis() + fileName;
            try {
                out = new FileOutputStream(filePath + newFileName);
                out.write(file.getBytes());
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            product.setImg("/myImg/" + newFileName);
        }
    }

}

