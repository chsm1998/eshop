package com.yhzl.two.shop.controller;


import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.ShopCart;
import com.yhzl.two.shop.entity.User;
import com.yhzl.two.shop.service.IShopCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chsm
 * @since 2018-09-07
 */
@RestController
@RequestMapping("/shopCart")
public class ShopCartController {

    private final IShopCartService shopCartService;

    @Autowired
    public ShopCartController(IShopCartService shopCartService) {
        this.shopCartService = shopCartService;
    }

    @PostMapping("/front/shopCart")
    public ServerResponse<String> add(@RequestBody ShopCart shopCart, @SessionAttribute(Const.LOGIN_USER) User user) {
        shopCart.setUid(user.getId());
        return shopCartService.add(shopCart);
    }

    @PutMapping("/front/shopCart")
    public ServerResponse<String> update(@RequestBody ShopCart shopCart) {
        return shopCartService.update(shopCart);
    }

    @GetMapping("/front/shopCarts")
    public ServerResponse<List<ShopCart>> getAll(@SessionAttribute(Const.LOGIN_USER) User user) {
        return shopCartService.queryByUid(user.getId());
    }

    @DeleteMapping("/front/shopCart")
    public ServerResponse<String> delete(Integer id) {
        return shopCartService.delete(id);
    }


}

