package com.yhzl.two.shop.dto;

import com.yhzl.two.shop.entity.Product;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-10 10:41
 **/
@Data
@Accessors(chain = true)
public class ShopCartDto {

    private Product product;
    private Integer num;

}
