package com.yhzl.two.shop.mapper;

import com.yhzl.two.shop.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
