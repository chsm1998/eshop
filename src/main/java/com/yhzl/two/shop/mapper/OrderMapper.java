package com.yhzl.two.shop.mapper;

import com.yhzl.two.shop.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface OrderMapper extends BaseMapper<Order> {

    /**
     * 通过用户id查询订单信息
     * @param uid   用户id
     * @return  订单信息
     */
    List<Order> queryByUid(@Param("uid") Integer uid);

}
