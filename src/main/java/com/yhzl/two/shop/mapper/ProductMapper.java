package com.yhzl.two.shop.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yhzl.two.shop.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface ProductMapper extends BaseMapper<Product> {

    IPage<Product> queryPage(IPage<Product> page, @Param("name") String name);

}
