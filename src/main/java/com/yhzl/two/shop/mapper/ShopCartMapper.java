package com.yhzl.two.shop.mapper;

import com.yhzl.two.shop.entity.ShopCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chsm
 * @since 2018-09-07
 */
public interface ShopCartMapper extends BaseMapper<ShopCart> {

    List<ShopCart> queryByUid(@Param("uid") Integer uid);

}
