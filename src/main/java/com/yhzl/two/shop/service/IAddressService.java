package com.yhzl.two.shop.service;

import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Address;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface IAddressService extends IService<Address> {

    /**
     * 添加地址
     * @param address   地址信息
     * @return  是否添加成功
     */
    ServerResponse<String> add(Address address);

    /**
     * 删除地址
     * @param id    地址id
     * @return  是否删除成功
     */
    ServerResponse<String> delete(Integer id);

    /**
     * 更新地址
     * @param address   地址信息
     * @return  是否更新成功
     */
    ServerResponse<String> update(Address address);

    /**
     * 通过用户id查询该用户的所有地址
     * @param uid 用户id
     * @return  地址集合信息
     */
    ServerResponse<List<Address>> queryAll(Integer uid);

}
