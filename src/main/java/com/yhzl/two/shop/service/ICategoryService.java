package com.yhzl.two.shop.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yhzl.two.shop.common.MyPage;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yhzl.two.shop.vo.CategoryVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface ICategoryService extends IService<Category> {

    /**
     * 添加类别
     * @param category  类别信息
     * @return  响应信息
     */
    ServerResponse<String> add(Category category);

    /**
     * 删除类别
     * @param id    类别id
     * @return  响应信息
     */
    ServerResponse<String> delete(Integer id);

    /**
     * 更新类别信息
     * @param category  类别信息
     * @return  响应信息
     */
    ServerResponse<String> update(Category category);

    /**
     * 分页查询
     * @param categoryVo    类别视图对象
     * @return  类别信息
     */
    ServerResponse<IPage<Category>> queryPage(CategoryVo categoryVo);

    /**
     * 通过名称查询类别
     * @param name  类别名称
     * @return  类别信息
     */
    ServerResponse<List<Category>> queryByName(String name);

    /**
     * 通过id查询类别信息
     * @param id    类别id
     * @return  类别信息
     */
    ServerResponse<Category> queryById(Integer id);

}
