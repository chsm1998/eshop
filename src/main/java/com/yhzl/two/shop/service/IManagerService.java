package com.yhzl.two.shop.service;

import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Manager;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface IManagerService extends IService<Manager> {

    /**
     * 后台登录
     * @param manager   管理者
     * @return  登录信息
     */
    ServerResponse<Manager> login(Manager manager);

}
