package com.yhzl.two.shop.service;

import com.yhzl.two.shop.entity.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface IOrderItemService extends IService<OrderItem> {

}
