package com.yhzl.two.shop.service;

import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yhzl.two.shop.vo.ShopCartVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface IOrderService extends IService<Order> {

    /**
     * 创建订单
     *
     * @param shopCartVo 购物车视图对象
     * @param uid        用户id
     * @return 是否创建成功
     */
    ServerResponse<Map<String, Boolean>> createOrder(ShopCartVo shopCartVo, Integer uid);

    /**
     * 通过用户id获取订单
     *
     * @param uid 用户id
     * @return 用户订单信息
     */
    ServerResponse<List<Order>> getOrderByUid(Integer uid);

    /**
     * 更新订单状态
     * @param id   订单id
     * @param state 订单状态
     * @return  状态信息
     */
    ServerResponse<String> updateState(Integer id, Integer state);
}
