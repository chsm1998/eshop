package com.yhzl.two.shop.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yhzl.two.shop.vo.ProductVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface IProductService extends IService<Product> {

    /**
     * 分页查询
     * @param productVo 商品vo对象
     * @return  商品集合
     */
    ServerResponse<IPage<Product>> queryPage(ProductVo productVo);

    /**
     * 通过id查询商品
     * @param id    商品id
     * @return  商品信息
     */
    ServerResponse<Product> queryById(Integer id);

    /**
     * 通过商品名称查询商品
     * @param name  商品名称
     * @return  商品信息列表
     */
    ServerResponse<List<Product>> queryByName(String name);

    /**
     * 通过商品名称和类型查询商品
     * @param product   商品信息
     * @return  商品信息列表
     */
    ServerResponse<List<Product>> queryByDescAndCid(Product product);

    /**
     * 添加商品
     * @param product   商品信息
     * @return  是否添加成功
     */
    ServerResponse<String> add(Product product);

    /**
     * 通过id删除商品
     * @param id    商品id
     * @return  是否删除成功
     */
    ServerResponse<String> deleteById(Integer id);

    /**
     * 通过id更新商品
     * @param product   商品信息
     * @return  是否更新成功
     */
    ServerResponse<String> update(Product product);

}
