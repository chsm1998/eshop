package com.yhzl.two.shop.service;

import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.ShopCart;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chsm
 * @since 2018-09-07
 */
public interface IShopCartService extends IService<ShopCart> {

    /**
     * 添加购物车
     * @param shopCart  购物车信息
     * @return  是否添加成功
     */
    ServerResponse<String> add(ShopCart shopCart);

    /**
     * 删除购物车
     * @param id  购物车id
     * @return  是否删除成功
     */
    ServerResponse<String> delete(Integer id);

    /**
     * 更新购物车信息
     * @param shopCart  购物车信息
     * @return  是否更新成功
     */
    ServerResponse<String> update(ShopCart shopCart);

    /**
     * 通过用户获取购物车信息
     * @param uid   用户id
     * @return  购物车信息
     */
    ServerResponse<List<ShopCart>> queryByUid(Integer uid);

}
