package com.yhzl.two.shop.service;

import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yhzl.two.shop.vo.UpdatePwdVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
public interface IUserService extends IService<User> {

    /**
     * 用户登录
     * @param user  用户对象
     * @return  响应状态
     */
    ServerResponse<User> login(User user);

    /**
     * 用户注册
     * @param user  用户对象
     * @return  响应状态
     */
    ServerResponse<String> register(User user);

    /**
     * 更新用户信息
     * @param user  用户信息
     * @return  新的用户信息
     */
    ServerResponse<User> updateUser(User user);

    /**
     * 更新用户密码
     * @param uid 用户id
     * @param updatePwdVo   密码视图
     * @return  是否更新成功
     */
    ServerResponse<String> updatePwd(Integer uid, UpdatePwdVo updatePwdVo);

    /**
     * 校验密码
     * @param pwd   用户密码
     * @return  是否校验通过
     */
    ServerResponse<String> checkPwd(Integer id, String pwd);

    /**
     * 校验用户名
     * @param username  用户名
     * @return  是否可用
     */
    ServerResponse<String> checkUsername(String username);

}
