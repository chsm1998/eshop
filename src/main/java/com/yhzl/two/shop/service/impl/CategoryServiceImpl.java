package com.yhzl.two.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yhzl.two.shop.common.MyPage;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.common.ServiceCommonMethod;
import com.yhzl.two.shop.entity.Category;
import com.yhzl.two.shop.mapper.CategoryMapper;
import com.yhzl.two.shop.service.ICategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yhzl.two.shop.vo.CategoryVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

    @Override
    public ServerResponse<String> add(Category category) {
        int count = baseMapper.insert(category);
        return ServiceCommonMethod.returnInsert(count, "添加类别成功", "添加类别失败,服务器异常");
    }

    @Override
    public ServerResponse<String> delete(Integer id) {
        int count = baseMapper.deleteById(id);
        return ServiceCommonMethod.returnInsert(count, "删除类别成功", "删除类别失败,服务器异常");
    }

    @Override
    public ServerResponse<String> update(Category category) {
        int count = baseMapper.updateById(category);
        return ServiceCommonMethod.returnInsert(count, "更新删除类别成功", "删除类别失败,服务器异常");
    }

    @Override
    public ServerResponse<IPage<Category>> queryPage(CategoryVo categoryVo) {
        IPage<Category> categoryIPage = baseMapper.selectPage(
                new Page<>(categoryVo.getMyPage().getCurrPage(),
                        categoryVo.getMyPage().getPageSize()),
                new QueryWrapper<Category>()
                        .like("name", categoryVo.getCategory().getName())
        );
        return ServerResponse.createSuccessData("获取类别信息成功", categoryIPage);
    }

    @Override
    public ServerResponse<List<Category>> queryByName(String name) {
        List<Category> categories = baseMapper.selectList(new QueryWrapper<Category>()
                .like("name", name));
        return ServerResponse.createSuccessData("获取信息成功", categories);
    }

    @Override
    public ServerResponse<Category> queryById(Integer id) {
        Category category = baseMapper.selectById(id);
        return ServerResponse.createSuccessData("获取类别信息成功", category);
    }
}
