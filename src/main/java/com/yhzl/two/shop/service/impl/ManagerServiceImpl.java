package com.yhzl.two.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yhzl.two.shop.common.MD5Util;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.entity.Manager;
import com.yhzl.two.shop.mapper.ManagerMapper;
import com.yhzl.two.shop.service.IManagerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@Service
public class ManagerServiceImpl extends ServiceImpl<ManagerMapper, Manager> implements IManagerService {

    @Override
    public ServerResponse<Manager> login(Manager manager) {
        Manager loginManager = baseMapper.selectOne(new QueryWrapper<Manager>()
                .eq("username", manager.getUsername())
                .eq("password", MD5Util.encrypt(manager.getPassword())));
        if (loginManager == null) {
            return ServerResponse.createErrorMessage("用户名或密码错误");
        }
        return ServerResponse.createSuccessData("登录成功", loginManager.setPassword(null));
    }
}
