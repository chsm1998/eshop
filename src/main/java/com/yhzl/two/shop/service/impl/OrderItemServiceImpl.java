package com.yhzl.two.shop.service.impl;

import com.yhzl.two.shop.entity.OrderItem;
import com.yhzl.two.shop.mapper.OrderItemMapper;
import com.yhzl.two.shop.service.IOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements IOrderItemService {

}
