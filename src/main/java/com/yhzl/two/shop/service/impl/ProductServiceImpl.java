package com.yhzl.two.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yhzl.two.shop.common.Const;
import com.yhzl.two.shop.common.ResponseCode;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.common.ServiceCommonMethod;
import com.yhzl.two.shop.entity.Product;
import com.yhzl.two.shop.mapper.ProductMapper;
import com.yhzl.two.shop.service.IProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yhzl.two.shop.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Override
    public ServerResponse<IPage<Product>> queryPage(ProductVo productVo) {
        IPage<Product> productIPage = baseMapper.queryPage(
                new Page<>(productVo.getMyPage().getCurrPage(),
                        productVo.getMyPage().getPageSize()), "%" + productVo.getProduct().getName() + "%");
        return ServerResponse.createSuccessData("获取信息成功", productIPage);
    }

    @Override
    public ServerResponse<Product> queryById(Integer id) {
        Product product = baseMapper.selectById(id);
        return ServerResponse.createSuccessData("获取信息成功", product);
    }

    @Override
    public ServerResponse<List<Product>> queryByName(String name) {
        List<Product> products = baseMapper.selectList(new QueryWrapper<Product>()
                .like("name", name));
        return ServerResponse.createSuccessData("获取信息成功", products);
    }

    @Override
    public ServerResponse<List<Product>> queryByDescAndCid(Product product) {
        QueryWrapper<Product> productWrapper = new QueryWrapper<>();
        productWrapper.like("pdesc", product.getPdesc());
        if (product.getCid() != null) {
            productWrapper.eq("cid", product.getCid());
        }
        List<Product> products = baseMapper.selectList(productWrapper);
        return ServerResponse.createSuccessData("获取信息成功", products);
    }

    @Override
    public ServerResponse<String> add(Product product) {
        int count = baseMapper.insert(product);
        return ServiceCommonMethod.returnInsert(count, "添加商品成功", "添加商品失败");
    }

    @Override
    public ServerResponse<String> deleteById(Integer id) {
        Product product = baseMapper.selectById(id);
        int count = baseMapper.deleteById(id);
        return ServiceCommonMethod.returnInsert(count, "删除商品成功", "删除商品失败");
    }

    @Override
    public ServerResponse<String> update(Product product) {
        Product oldProduct = baseMapper.selectById(product);
        int count = baseMapper.updateById(product);
        ServerResponse<String> response = ServiceCommonMethod.returnInsert(count, "更新商品成功", "更新商品失败");
        return response;
    }

    /**
     * 删除图片
     * 该方法不应再调用，对于商品图片，应永久保存否则会导致删除的商品订单明细
     * 无法显示图片
     * @param response 响应信息
     * @param product  商品信息
     */
    @Deprecated
    private void deleteImg(ServerResponse<String> response, Product product) {
        if (response.getStatus() == ResponseCode.SUCCESS.getCode()) {
            String img = product.getImg();
            img = img.substring(img.indexOf("/", 2), img.length());
            String filePath = Const.FILE_PATH + img;
            File file = new File(filePath);
            System.out.println(img);
            file.delete();
        }
    }
}
