package com.yhzl.two.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yhzl.two.shop.common.MD5Util;
import com.yhzl.two.shop.common.ServerResponse;
import com.yhzl.two.shop.common.ServiceCommonMethod;
import com.yhzl.two.shop.common.UserState;
import com.yhzl.two.shop.entity.User;
import com.yhzl.two.shop.mapper.UserMapper;
import com.yhzl.two.shop.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yhzl.two.shop.vo.UpdatePwdVo;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chsm
 * @since 2018-09-04
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public ServerResponse<User> login(User user) {
        // 校验用户名密码
        User loginUser = baseMapper.selectOne(new QueryWrapper<User>()
                .eq("username", user.getUsername())
                // md5加密
                .eq("password", MD5Util.encrypt(user.getPassword())));
        if (loginUser == null) {
            return ServerResponse.createErrorMessage("用户名或密码错误");
        }
        return ServerResponse.createSuccessData("登录成功", loginUser);
    }

    @Override
    public ServerResponse<String> register(User user) {
        // 加密密码
        user.setPassword(MD5Util.encrypt(user.getPassword()));
        // 设置状态
        user.setState(UserState.REGISTER.getCode());
        int count = baseMapper.insert(user);
        return ServiceCommonMethod.returnInsert(count, "注册成功", "注册失败，服务器异常");
    }

    @Override
    public ServerResponse<User> updateUser(User user) {
        // 将状态设置为注册状态，避免恶意提交
        user.setState(UserState.REGISTER.getCode());
        int count = baseMapper.updateById(user);
        if (count > 0) {
            return ServerResponse.createSuccessData("更新用户信息成功", user);
        }
        return ServerResponse.createErrorMessage("更新用户信息失败");
    }

    @Override
    public ServerResponse<String> updatePwd(Integer uid, UpdatePwdVo updatePwdVo) {
        User user = baseMapper.selectById(uid);
        if (MD5Util.encrypt(updatePwdVo.getPwd()).equals(user.getPassword())) {
            baseMapper.updateById(user.setPassword(MD5Util.encrypt(updatePwdVo.getNewPwd())));
            return ServerResponse.createSuccessMessage("密码更新成功");
        }
        return ServerResponse.createErrorMessage("密码校验未通过");
    }

    @Override
    public ServerResponse<String> checkPwd(Integer id, String pwd) {
        User user = baseMapper.selectById(id);
        if (MD5Util.encrypt(pwd).equals(user.getPassword())) {
            return ServerResponse.createSuccessMessage("密码校验通过");
        }
        return ServerResponse.createErrorMessage("密码校验未通过");
    }

    @Override
    public ServerResponse<String> checkUsername(String username) {
        User user = baseMapper.selectOne(new QueryWrapper<User>()
                .eq("username", username));
        if (user == null) {
            return ServerResponse.createSuccessMessage("用户名可用");
        }
        return ServerResponse.createErrorMessage("用户名不可用");
    }
}
