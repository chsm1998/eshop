package com.yhzl.two.shop.vo;

import com.yhzl.two.shop.common.MyPage;
import com.yhzl.two.shop.entity.Category;
import lombok.Data;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-05 14:02
 **/
@Data
public class CategoryVo {

    private Category category;
    private MyPage myPage;

}
