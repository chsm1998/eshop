package com.yhzl.two.shop.vo;

import lombok.Data;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-14 08:57
 **/
@Data
public class OrderStateVo {

    private Integer oid;
    private Integer state;

}
