package com.yhzl.two.shop.vo;

import com.yhzl.two.shop.common.MyPage;
import com.yhzl.two.shop.entity.Product;
import lombok.Data;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-05 21:26
 **/
@Data
public class ProductVo {

    private Product product;
    private MyPage myPage;

}
