package com.yhzl.two.shop.vo;

import lombok.Data;

import java.util.List;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-11 15:03
 **/
@Data
public class ShopCartVo {

    private List<Integer> sid;
    private Integer addressId;

}
