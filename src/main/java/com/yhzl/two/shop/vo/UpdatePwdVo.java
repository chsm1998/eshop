package com.yhzl.two.shop.vo;

import lombok.Data;

/**
 * @program: shop
 * @author: chsm
 * @create: 2018-09-19 10:18
 **/
@Data
public class UpdatePwdVo {

    private String pwd;
    private String newPwd;

}
