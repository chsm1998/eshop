/*
Navicat MySQL Data Transfer

Source Server         : studymysql
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : two_shop

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-12-15 17:14:41
*/
CREATE USER 'three'@'localhost' IDENTIFIED BY 'team';
GRANT ALL ON *.* TO 'three'@'localhost';
SET FOREIGN_KEY_CHECKS=0;
CREATE DATABASE two_shop;
USE two_shop;

-- ----------------------------
-- Table structure for `address`
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `province` varchar(20) NOT NULL COMMENT '省份',
  `city` varchar(20) NOT NULL COMMENT '市',
  `county` varchar(20) NOT NULL COMMENT '县',
  `area` varchar(20) DEFAULT NULL COMMENT '区',
  `detailed` varchar(200) NOT NULL COMMENT '详细地址',
  `is_default` bit(1) NOT NULL COMMENT '是否为默认地址',
  `name` varchar(20) NOT NULL COMMENT '收货人名称',
  `tel` varchar(20) NOT NULL COMMENT '收货人电话',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('2', '3', '110000', '110000', '110108', null, '可行路666号', '', '王珂新', '15699887766', '2018-09-11 10:26:03', '2018-09-25 14:36:07');
INSERT INTO `address` VALUES ('3', '3', '360000', '360800', '360830', null, '不知道路233号', '', '刘强', '15779623464', '2018-09-11 10:28:01', '2018-09-19 11:02:24');
INSERT INTO `address` VALUES ('10', '3', '110000', '110000', '110109', null, '江西省上饶市鄱阳县天祥大道388号江西工业职业技术学院', '', '10086', '156545', '2018-09-11 10:48:34', '2018-09-25 14:36:07');
INSERT INTO `address` VALUES ('12', '3', '140000', '140600', '140622', null, '测试地址', '', '测试', '15875668457', '2018-09-19 09:57:37', '2018-09-19 09:57:40');
INSERT INTO `address` VALUES ('13', '3', '120000', '120000', '120105', null, '测试地址', '', '测试地址', '15876998547', '2018-09-20 09:16:38', '2018-09-20 09:16:41');
INSERT INTO `address` VALUES ('14', '6', '140000', '140500', '140525', null, '泽州区泽州路666号', '', '测试人员', '15788669854', '2018-09-25 08:51:49', '2018-09-25 08:51:49');
INSERT INTO `address` VALUES ('15', '3', '120000', '120000', '120106', null, '测试地址', '', '测试', '13422224455', '2018-09-25 14:34:02', '2018-09-25 14:35:24');

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `name` varchar(10) NOT NULL COMMENT '类别名称',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('16', '测试类', '2018-09-06 10:28:45', '2018-09-06 15:19:52');
INSERT INTO `category` VALUES ('17', '数码类', '2018-09-06 15:43:23', '2018-09-06 15:43:23');
INSERT INTO `category` VALUES ('18', '图书类', '2018-09-06 15:43:32', '2018-09-06 15:43:32');
INSERT INTO `category` VALUES ('19', '食品类', '2018-09-06 15:43:46', '2018-09-06 15:43:46');
INSERT INTO `category` VALUES ('20', '服装类', '2018-09-06 15:44:25', '2018-09-06 15:44:25');
INSERT INTO `category` VALUES ('21', '酒水类', '2018-09-06 15:44:38', '2018-09-06 15:44:38');

-- ----------------------------
-- Table structure for `manager`
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `name` varchar(20) NOT NULL COMMENT '昵称',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES ('1', '系统管理员', 'hello', '89D20EE96F18D9EAA7B9D28F04E06F91');

-- ----------------------------
-- Table structure for `order_item`
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `num` int(11) NOT NULL COMMENT '商品数量',
  `price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `img` varchar(100) NOT NULL COMMENT '商品图片',
  `name` varchar(100) NOT NULL COMMENT '商品名称',
  `oid` varchar(20) NOT NULL COMMENT '订单id',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES ('20', '10', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '2477596061212993', '2018-09-13 09:57:33', '2018-09-13 09:57:33');
INSERT INTO `order_item` VALUES ('21', '1', '2699.00', '/myImg/1536282522887TB1o8kYaz7nBKNjSZLeXXbxCFXa.jpg_480x480Q90.jpg_.webp', 'OPPO R15手机 超视野全面屏， AI智能拍照， 让美更自然', '2477596061212993', '2018-09-13 09:57:33', '2018-09-13 09:57:33');
INSERT INTO `order_item` VALUES ('22', '10', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '2478562570340815', '2018-09-13 10:13:40', '2018-09-13 10:13:40');
INSERT INTO `order_item` VALUES ('23', '1', '2699.00', '/myImg/1536282522887TB1o8kYaz7nBKNjSZLeXXbxCFXa.jpg_480x480Q90.jpg_.webp', 'OPPO R15手机 超视野全面屏， AI智能拍照， 让美更自然', '2478562570340815', '2018-09-13 10:13:40', '2018-09-13 10:13:40');
INSERT INTO `order_item` VALUES ('24', '10', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '2478729832889613', '2018-09-13 10:16:27', '2018-09-13 10:16:27');
INSERT INTO `order_item` VALUES ('25', '1', '2699.00', '/myImg/1536282522887TB1o8kYaz7nBKNjSZLeXXbxCFXa.jpg_480x480Q90.jpg_.webp', 'OPPO R15手机 超视野全面屏， AI智能拍照， 让美更自然', '2478729832889613', '2018-09-13 10:16:27', '2018-09-13 10:16:27');
INSERT INTO `order_item` VALUES ('26', '10', '16.80', '/myImg/1536282041842TB2JFKFwbsrBKNjSZFpXXcXhFXa_!!725677994-0-item_pic.jpg_580x580Q90.jpg_.webp', '三只松鼠 猪肉脯210g休闲零食小吃特产靖江风味肉干', '2479896235912373', '2018-09-13 10:35:53', '2018-09-13 10:35:53');
INSERT INTO `order_item` VALUES ('27', '1', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '2479896235912373', '2018-09-13 10:35:53', '2018-09-13 10:35:53');
INSERT INTO `order_item` VALUES ('28', '1', '2699.00', '/myImg/1536282522887TB1o8kYaz7nBKNjSZLeXXbxCFXa.jpg_480x480Q90.jpg_.webp', 'OPPO R15手机 超视野全面屏， AI智能拍照， 让美更自然', '2479896235912373', '2018-09-13 10:35:53', '2018-09-13 10:35:53');
INSERT INTO `order_item` VALUES ('29', '1', '68.90', '/myImg/1536282797138TB2bdhOv5MnBKNjSZFoXXbOSFXa_!!1751831526-0-item_pic.jpg_580x580Q90.jpg_.webp', '课外书10-15岁五六年级必读儿童读物小学生3-6年级阅读8-9-12岁爱的教育绿山墙的', '2479896235912373', '2018-09-13 10:35:53', '2018-09-13 10:35:53');
INSERT INTO `order_item` VALUES ('30', '100', '1600.00', '/myImg/1536282429234TB13l5MhqAoBKNjSZSyXXaHAVXa.jpg_480x480Q90.jpg_.webp', 'vivo Z1 3D炫彩流光机身， 后置1300万AI智慧双摄手机', '2492821103455957', '2018-09-13 14:11:18', '2018-09-13 14:11:18');
INSERT INTO `order_item` VALUES ('31', '23', '69.90', '/myImg/1536281705447TB13emOvYZnBKNjSZFGXXbt3FXa_!!0-item_pic.jpg_580x580Q90.jpg_.webp', '骄子牧场内蒙古风干手撕牛肉干1斤装麻辣味500g散装零食小吃熟食', '2494573891653240', '2018-09-13 14:40:31', '2018-09-13 14:40:31');
INSERT INTO `order_item` VALUES ('32', '19', '2300.00', '/myImg/1536283192882TB1OVumi8jTBKNjSZFwXXcG4XXa.jpg_480x480Q90.jpg_.webp', '小米 小米手机8 超清四曲面， AI超感光双摄， 红外人脸识别', '2494573891653240', '2018-09-13 14:40:31', '2018-09-13 14:40:31');
INSERT INTO `order_item` VALUES ('33', '1', '2699.00', '/myImg/1536282522887TB1o8kYaz7nBKNjSZLeXXbxCFXa.jpg_480x480Q90.jpg_.webp', 'OPPO R15手机 超视野全面屏， AI智能拍照， 让美更自然', '2494573891653240', '2018-09-13 14:40:31', '2018-09-13 14:40:31');
INSERT INTO `order_item` VALUES ('34', '5', '88.00', '/myImg/1536282954023TB2E3i2D3mTBuNjy1XbXXaMrVXa_!!217042976-0-item_pic.jpg_580x580Q90.jpg_.webp', '现货包邮深入理解JAVA虚拟机JVM高级特性与最佳实践(第2版)/Java入门', '2494573891653240', '2018-09-13 14:40:31', '2018-09-13 14:40:31');
INSERT INTO `order_item` VALUES ('35', '2', '1600.00', '/myImg/1536282429234TB13l5MhqAoBKNjSZSyXXaHAVXa.jpg_480x480Q90.jpg_.webp', 'vivo Z1 3D炫彩流光机身， 后置1300万AI智慧双摄手机', '2499243970973490', '2018-09-13 15:58:21', '2018-09-13 15:58:21');
INSERT INTO `order_item` VALUES ('36', '1', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '2499243970973490', '2018-09-13 15:58:21', '2018-09-13 15:58:21');
INSERT INTO `order_item` VALUES ('37', '98', '1600.00', '/myImg/1536282429234TB13l5MhqAoBKNjSZSyXXaHAVXa.jpg_480x480Q90.jpg_.webp', 'vivo Z1 3D炫彩流光机身， 后置1300万AI智慧双摄手机', '2499358223886052', '2018-09-13 16:00:15', '2018-09-13 16:00:15');
INSERT INTO `order_item` VALUES ('38', '32', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '2499586990557841', '2018-09-13 16:04:04', '2018-09-13 16:04:04');
INSERT INTO `order_item` VALUES ('39', '1', '68.90', '/myImg/1536282797138TB2bdhOv5MnBKNjSZFoXXbOSFXa_!!1751831526-0-item_pic.jpg_580x580Q90.jpg_.webp', '课外书10-15岁五六年级必读儿童读物小学生3-6年级阅读8-9-12岁爱的教育绿山墙的', '2499586990557841', '2018-09-13 16:04:04', '2018-09-13 16:04:04');
INSERT INTO `order_item` VALUES ('40', '1', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '2563807761292775', '2018-09-14 09:54:13', '2018-09-14 09:54:13');
INSERT INTO `order_item` VALUES ('41', '1', '2699.00', '/myImg/1536282522887TB1o8kYaz7nBKNjSZLeXXbxCFXa.jpg_480x480Q90.jpg_.webp', 'OPPO R15手机 超视野全面屏， AI智能拍照， 让美更自然', '2563807761292775', '2018-09-14 09:54:13', '2018-09-14 09:54:13');
INSERT INTO `order_item` VALUES ('42', '50', '19.90', '/myImg/1536282305383TB2vkxruoOWBKNjSZKzXXXfWFXa_!!725677994-0-sm.jpg_580x580Q90.jpg_.webp', '乐事无限薯片三连罐104g*3罐休闲食品零食小吃礼包膨化', '2568498209372608', '2018-09-14 11:12:23', '2018-09-14 11:12:23');
INSERT INTO `order_item` VALUES ('43', '1', '2300.00', '/myImg/1536283192882TB1OVumi8jTBKNjSZFwXXcG4XXa.jpg_480x480Q90.jpg_.webp', '小米 小米手机8 超清四曲面， AI超感光双摄， 红外人脸识别', '94193697727192', '2018-09-18 16:18:43', '2018-09-18 16:18:43');
INSERT INTO `order_item` VALUES ('44', '1', '2300.00', '/myImg/1536283192882TB1OVumi8jTBKNjSZFwXXcG4XXa.jpg_480x480Q90.jpg_.webp', '小米 小米手机8 超清四曲面， AI超感光双摄， 红外人脸识别', '241909430997544', '2018-09-20 09:19:53', '2018-09-20 09:19:53');
INSERT INTO `order_item` VALUES ('45', '1', '2699.00', '/myImg/1536282522887TB1o8kYaz7nBKNjSZLeXXbxCFXa.jpg_480x480Q90.jpg_.webp', 'OPPO R15手机 超视野全面屏， AI智能拍照， 让美更自然', '245591808552726', '2018-09-20 10:21:16', '2018-09-20 10:21:16');
INSERT INTO `order_item` VALUES ('46', '1', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '211274868008365', '2018-09-25 08:51:52', '2018-09-25 08:51:52');
INSERT INTO `order_item` VALUES ('47', '1', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '211865673872814', '2018-09-25 09:01:42', '2018-09-25 09:01:42');
INSERT INTO `order_item` VALUES ('48', '10', '208.00', '/myImg/1536282854491TB18Ga0pol7MKJjSZFDXXaOEpXa_!!0-item_pic.jpg_580x580Q90.jpg_.webp', 'Core Java 核心技术 卷I+卷II 基础知识+高级特性 第十版10版中文版 依据Java SE8 ', '211865673872814', '2018-09-25 09:01:42', '2018-09-25 09:01:42');
INSERT INTO `order_item` VALUES ('49', '10', '68.00', '/myImg/1536283392653TB23MBHbWZPyuJjSspmXXX2IXXa_!!0-saturn_solar.jpg_580x580Q90.jpg_.webp', '免烫职业正装长袖男衬衫商务上班白衬衣工作制服工装定制刺绣logo', '211865673872814', '2018-09-25 09:01:42', '2018-09-25 09:01:42');
INSERT INTO `order_item` VALUES ('50', '1', '2300.00', '/myImg/1536283192882TB1OVumi8jTBKNjSZFwXXcG4XXa.jpg_480x480Q90.jpg_.webp', '小米 小米手机8 超清四曲面， AI超感光双摄， 红外人脸识别', '231606146515316', '2018-09-25 14:30:43', '2018-09-25 14:30:43');
INSERT INTO `order_item` VALUES ('51', '1', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '231812815580760', '2018-09-25 14:34:10', '2018-09-25 14:34:10');
INSERT INTO `order_item` VALUES ('52', '1', '1999.00', '/myImg/1536282568196TB1a3eql8smBKNjSZFFXXcT9VXa.jpg_480x480Q90.jpg_.webp', '华为 nova 3i手机 双摄美拍， 智慧场景识别', '231812815580760', '2018-09-25 14:34:10', '2018-09-25 14:34:10');
INSERT INTO `order_item` VALUES ('53', '1', '444.00', '/myImg/153785711566321.jpg', '测试', '231812815580760', '2018-09-25 14:34:10', '2018-09-25 14:34:10');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `name` varchar(20) NOT NULL COMMENT '商品名称',
  `price` decimal(10,2) NOT NULL COMMENT '价格',
  `img` varchar(200) NOT NULL COMMENT '商品图片',
  `pdesc` varchar(200) NOT NULL COMMENT '商品描述',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '商品状态',
  `stock` int(11) NOT NULL COMMENT '商品库存',
  `cid` int(11) NOT NULL COMMENT '类别id',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('29', '网红小吃', '69.99', '/myImg/1536281602063TB23KvVor5YBuNjSspoXXbeNFXa_!!0-saturn_solar.jpg_580x580Q90.jpg_.webp', '进口猪饲料零食大礼包组合整箱超大混装实惠网红小吃生日一箱休闲', '0', '1250', '19', '2018-09-07 08:53:22', '2018-09-07 14:42:58');
INSERT INTO `product` VALUES ('30', '三只松鼠-猪肉脯', '16.80', '/myImg/1536282041842TB2JFKFwbsrBKNjSZFpXXcXhFXa_!!725677994-0-item_pic.jpg_580x580Q90.jpg_.webp', '三只松鼠 猪肉脯210g休闲零食小吃特产靖江风味肉干', '0', '1000', '19', '2018-09-07 08:54:17', '2018-09-07 09:00:42');
INSERT INTO `product` VALUES ('31', '风干手撕牛肉', '69.90', '/myImg/1536281705447TB13emOvYZnBKNjSZFGXXbt3FXa_!!0-item_pic.jpg_580x580Q90.jpg_.webp', '骄子牧场内蒙古风干手撕牛肉干1斤装麻辣味500g散装零食小吃熟食', '0', '158', '19', '2018-09-07 08:55:05', '2018-09-20 10:11:56');
INSERT INTO `product` VALUES ('32', '良品铺子-麻薯组合', '36.90', '/myImg/1536281901316TB2nlL2tY3nBKNjSZFMXXaUSFXa_!!725677994-0-item_pic.jpg_580x580Q90.jpg_.webp', '【下单立减5元】良品铺子麻薯组合1050g芒果抹茶味糕点零食大礼包', '0', '1578', '19', '2018-09-07 08:58:21', '2018-09-07 08:58:21');
INSERT INTO `product` VALUES ('33', '乐事薯片三连桶', '19.90', '/myImg/1536282305383TB2vkxruoOWBKNjSZKzXXXfWFXa_!!725677994-0-sm.jpg_580x580Q90.jpg_.webp', '乐事无限薯片三连罐104g*3罐休闲食品零食小吃礼包膨化', '0', '105', '19', '2018-09-07 09:05:05', '2018-09-14 11:12:23');
INSERT INTO `product` VALUES ('34', '小米8', '2300.00', '/myImg/1536283192882TB1OVumi8jTBKNjSZFwXXcG4XXa.jpg_480x480Q90.jpg_.webp', '小米 小米手机8 超清四曲面， AI超感光双摄， 红外人脸识别', '0', '997', '17', '2018-09-07 09:06:24', '2018-09-25 14:30:43');
INSERT INTO `product` VALUES ('35', 'vivo z1', '1600.00', '/myImg/1536282429234TB13l5MhqAoBKNjSZSyXXaHAVXa.jpg_480x480Q90.jpg_.webp', 'vivo Z1 3D炫彩流光机身， 后置1300万AI智慧双摄手机', '0', '0', '17', '2018-09-07 09:07:09', '2018-09-13 16:00:15');
INSERT INTO `product` VALUES ('36', 'iphone x', '6999.00', '/myImg/1536283204321TB1nYk_aGLN8KJjSZFGXXbjrVXa.jpg_480x480Q90.jpg_.webp', '苹果 iPhone X 面容ID识别， 前置深感摄像头， 人像光效手机', '0', '963', '17', '2018-09-07 09:07:55', '2018-09-25 14:34:10');
INSERT INTO `product` VALUES ('37', 'OPPO R15', '2699.00', '/myImg/1536282522887TB1o8kYaz7nBKNjSZLeXXbxCFXa.jpg_480x480Q90.jpg_.webp', 'OPPO R15手机 超视野全面屏， AI智能拍照， 让美更自然', '0', '152', '17', '2018-09-07 09:08:43', '2018-09-20 10:21:16');
INSERT INTO `product` VALUES ('38', '华为 nova 3i', '1999.00', '/myImg/1536282568196TB1a3eql8smBKNjSZFFXXcT9VXa.jpg_480x480Q90.jpg_.webp', '华为 nova 3i手机 双摄美拍， 智慧场景识别', '0', '1044', '17', '2018-09-07 09:09:28', '2018-09-25 14:34:10');
INSERT INTO `product` VALUES ('39', '墨菲定律启示录', '18.80', '/myImg/1536282752257TB2aLsYppooBKNjSZFPXXXa2XXa_!!0-saturn_solar.jpg_580x580Q90.jpg_.webp', '心理学书籍读心术受益一生的墨菲定律 心理学书籍 墨菲定律正版 职场谈判人际交往', '0', '1578', '18', '2018-09-07 09:12:32', '2018-09-07 09:12:32');
INSERT INTO `product` VALUES ('40', '少儿必读图书', '68.90', '/myImg/1536282797138TB2bdhOv5MnBKNjSZFoXXbOSFXa_!!1751831526-0-item_pic.jpg_580x580Q90.jpg_.webp', '课外书10-15岁五六年级必读儿童读物小学生3-6年级阅读8-9-12岁爱的教育绿山墙的', '0', '1577', '18', '2018-09-07 09:13:17', '2018-09-13 16:04:04');
INSERT INTO `product` VALUES ('41', 'java核心技术', '208.00', '/myImg/1536282854491TB18Ga0pol7MKJjSZFDXXaOEpXa_!!0-item_pic.jpg_580x580Q90.jpg_.webp', 'Core Java 核心技术 卷I+卷II 基础知识+高级特性 第十版10版中文版 依据Java SE8 ', '0', '1844', '18', '2018-09-07 09:14:15', '2018-09-25 09:01:42');
INSERT INTO `product` VALUES ('42', '剑指offer', '68.90', '/myImg/1536282907878TB1WpMggeGSBuNjSspbXXciipXa_!!0-item_pic.jpg_580x580Q90.jpg_.webp', '剑指Offer：名企面试官精讲典型编程题（第2版）新增大量面试题 程序员面试宝典 ', '0', '1884', '18', '2018-09-07 09:15:08', '2018-09-07 09:15:08');
INSERT INTO `product` VALUES ('43', '深入理解java虚拟机', '88.00', '/myImg/1536282954023TB2E3i2D3mTBuNjy1XbXXaMrVXa_!!217042976-0-item_pic.jpg_580x580Q90.jpg_.webp', '现货包邮深入理解JAVA虚拟机JVM高级特性与最佳实践(第2版)/Java入门', '0', '175', '18', '2018-09-07 09:15:54', '2018-09-07 09:15:54');
INSERT INTO `product` VALUES ('44', '企业定制衫', '68.00', '/myImg/1536283392653TB23MBHbWZPyuJjSspmXXX2IXXa_!!0-saturn_solar.jpg_580x580Q90.jpg_.webp', '免烫职业正装长袖男衬衫商务上班白衬衣工作制服工装定制刺绣logo', '0', '1865', '20', '2018-09-07 09:23:13', '2018-09-25 09:01:42');
INSERT INTO `product` VALUES ('45', '抖音同款网红熊', '168.00', '/myImg/1536284080258TB27V.jybGYBuNjy0FoXXciBFXa_!!2901230117.jpg_580x580Q90.jpg_ (1).webp', '网红熊装抖音熊人偶服装成人行走传单熊求婚道具布朗熊卡通玩偶服', '0', '188', '20', '2018-09-07 09:24:05', '2018-09-07 09:34:47');
INSERT INTO `product` VALUES ('46', '欧昵雪2018秋装新款', '179.00', '/myImg/1536284177154TB21SNEv_qWBKNjSZFAXXanSpXa_!!0-saturn_solar.jpg_580x580Q90.jpg_.webp', '欧昵雪2018秋装新款OL撞色圆点印花立领衬衫女上衣波点长袖衬衣', '0', '15555', '20', '2018-09-07 09:36:17', '2018-09-07 09:36:17');
INSERT INTO `product` VALUES ('47', ' 卫衣女春秋装', '68.00', '/myImg/1536284215953TB1ETUgbXmWBuNjSspdXXbugXXa_!!0-item_pic.jpg_580x580Q90.jpg_.webp', ' 卫衣女春秋装2018新款长袖韩版宽松外套上衣酷酷的衣服秋天连帽衫', '0', '1554', '20', '2018-09-07 09:36:56', '2018-09-07 09:36:56');
INSERT INTO `product` VALUES ('48', '背心长裙外穿打底', '178.00', '/myImg/1536284252419TB22N38DKuSBuNjSsplXXbe8pXa_!!2217148345-0-item_pic.jpg_580x580Q90.jpg_.webp', '背心长裙外穿打底中长款无袖吊带连衣裙女装春秋夏季2018新款衣服', '0', '1478', '20', '2018-09-07 09:37:32', '2018-09-07 09:37:32');
INSERT INTO `product` VALUES ('50', '111', '888.00', '/myImg/153783690826018.jpg', '111', '0', '156', '23', '2018-09-25 08:55:08', '2018-09-25 08:55:08');
INSERT INTO `product` VALUES ('51', '测试', '444.00', '/myImg/153785711566321.jpg', '测试', '0', '0', '16', '2018-09-25 14:31:56', '2018-09-25 14:34:10');
INSERT INTO `product` VALUES ('52', '测试22', '666.00', '/myImg/153785717562117.jpg', '测试222', '0', '166', '18', '2018-09-25 14:32:56', '2018-09-25 14:32:56');

-- ----------------------------
-- Table structure for `shop_cart`
-- ----------------------------
DROP TABLE IF EXISTS `shop_cart`;
CREATE TABLE `shop_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `pid` int(11) NOT NULL COMMENT '商品id',
  `num` int(11) NOT NULL COMMENT '商品数量',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_cart
-- ----------------------------
INSERT INTO `shop_cart` VALUES ('3', '6', '38', '1', '2018-09-25 09:01:09', '2018-09-25 09:01:09');
INSERT INTO `shop_cart` VALUES ('11', '3', '34', '3', '2018-09-25 16:38:00', '2018-09-25 16:43:42');

-- ----------------------------
-- Table structure for `shop_order`
-- ----------------------------
DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE `shop_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `oid` varchar(100) NOT NULL COMMENT '订单id',
  `sum_price` decimal(10,2) NOT NULL COMMENT '订单总金额',
  `address` varchar(100) NOT NULL COMMENT '订单地址',
  `tel` varchar(20) NOT NULL COMMENT '收货人电话',
  `name` varchar(20) NOT NULL COMMENT '收货人姓名',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `state` tinyint(2) NOT NULL COMMENT '订单状态',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_order
-- ----------------------------
INSERT INTO `shop_order` VALUES ('22', '2477596061212993', '9698.00', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '1', '2018-09-13 09:57:33', '2018-09-14 09:49:45');
INSERT INTO `shop_order` VALUES ('23', '2478562570340815', '9698.00', '110000110000110108可行路666号', '15699887766', '王珂新', '3', '1', '2018-09-13 10:13:40', '2018-09-14 09:49:44');
INSERT INTO `shop_order` VALUES ('24', '2478729832889613', '9698.00', '110000110000110109江西省上饶市鄱阳县天祥大道388号江西工业职业技术学院', '156545', '10086', '3', '1', '2018-09-13 10:16:27', '2018-09-13 10:16:27');
INSERT INTO `shop_order` VALUES ('25', '2479896235912373', '9783.70', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '4', '2018-09-13 10:35:53', '2018-09-14 09:54:55');
INSERT INTO `shop_order` VALUES ('26', '2492821103455957', '1600.00', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '3', '2018-09-13 14:11:18', '2018-09-14 09:54:44');
INSERT INTO `shop_order` VALUES ('27', '2494573891653240', '5156.90', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '4', '2018-09-13 14:40:31', '2018-09-14 09:51:44');
INSERT INTO `shop_order` VALUES ('28', '2499243970973490', '8599.00', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '3', '2018-09-13 15:58:21', '2018-09-14 09:51:29');
INSERT INTO `shop_order` VALUES ('29', '2499358223886052', '1600.00', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '3', '2018-09-13 16:00:15', '2018-09-14 09:51:35');
INSERT INTO `shop_order` VALUES ('30', '2499586990557841', '7067.90', '14000014060014062145', '3', '测试', '3', '3', '2018-09-13 16:04:04', '2018-09-14 09:54:43');
INSERT INTO `shop_order` VALUES ('31', '2563807761292775', '9698.00', '110000110000110108可行路666号', '15699887766', '王珂新', '3', '3', '2018-09-14 09:54:12', '2018-09-14 09:54:42');
INSERT INTO `shop_order` VALUES ('32', '2568498209372608', '19.90', '110000110000110108可行路666号', '15699887766', '王珂新', '3', '1', '2018-09-14 11:12:23', '2018-09-14 11:12:23');
INSERT INTO `shop_order` VALUES ('33', '94193697727192', '2300.00', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '1', '2018-09-18 16:18:42', '2018-09-19 11:03:31');
INSERT INTO `shop_order` VALUES ('34', '241909430997544', '2300.00', '110000110000110108可行路666号', '15699887766', '王珂新', '3', '3', '2018-09-20 09:19:53', '2018-09-20 10:28:21');
INSERT INTO `shop_order` VALUES ('35', '245591808552726', '2699.00', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '2', '2018-09-20 10:21:16', '2018-09-20 10:28:26');
INSERT INTO `shop_order` VALUES ('36', '211274868008365', '6999.00', '140000140500140525泽州区泽州路666号', '15788669854', '测试人员', '6', '3', '2018-09-25 08:51:52', '2018-09-25 08:52:20');
INSERT INTO `shop_order` VALUES ('37', '211865673872814', '7275.00', '140000140500140525泽州区泽州路666号', '15788669854', '测试人员', '6', '4', '2018-09-25 09:01:42', '2018-09-25 09:02:04');
INSERT INTO `shop_order` VALUES ('38', '231606146515316', '2300.00', '360000360800360830不知道路233号', '15779623464', '刘强', '3', '3', '2018-09-25 14:30:43', '2018-09-25 14:31:26');
INSERT INTO `shop_order` VALUES ('39', '231812815580760', '9442.00', '120000120000120106测试地址', '13422224455', '测试', '3', '1', '2018-09-25 14:34:09', '2018-09-25 14:34:09');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id，主键',
  `username` varchar(30) NOT NULL COMMENT '用户名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `role_name` varchar(20) NOT NULL COMMENT '角色名',
  `name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `email` varchar(30) NOT NULL COMMENT '电子邮箱',
  `tel` varchar(20) NOT NULL COMMENT '电话',
  `birthday` date DEFAULT NULL,
  `sex` bit(1) NOT NULL COMMENT '性别',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '账号状态',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', 'admin', 'ADFC403EB0B1DF5413EA9D8C23B53A22', '永恒之恋', null, '1978364643@qq.com', '15216054524', null, '', '0', '2018-09-04 11:14:10', '2018-09-04 11:14:10');
INSERT INTO `user` VALUES ('3', 'hello', '5225F2DBC46F9C98EADC922083A86940', '测试人员', '刘强', '15788564525@qq.com', '17548556685', '1990-07-19', '', '0', '2018-09-04 11:29:38', '2018-09-20 09:17:08');
INSERT INTO `user` VALUES ('4', 'Alibaba', '4C934FD2D93B7210621732F337B9F6E0', 'Alibaba', '永恒之恋', 'Alibaba@qq.com', '15874556875', '2010-06-09', '', '0', '2018-09-05 09:03:15', '2018-09-05 09:03:15');
INSERT INTO `user` VALUES ('5', 'test', 'CBEBD942E9C9FBC7940B2CFBBA0A20D5', '永恒之恋', '测试', '1978364643@qq.com', '13479668548', '2010-07-13', '', '0', '2018-09-16 09:46:40', '2018-09-16 09:46:40');
INSERT INTO `user` VALUES ('6', 'test999', 'CBEBD942E9C9FBC7940B2CFBBA0A20D5', 'test666', '测试', 'test@qq.com', '15278896685', '2018-09-12', '', '0', '2018-09-25 08:50:26', '2018-09-25 08:50:52');
